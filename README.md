# Erdbeschleunigung mit dem Handy

Additional file for mobile phone app phyphox, to calculate the average g-force.

Installation
Zuerst phyphox auf das Handy laden.
Dann die Datei Erdbeschleunigung.phyphox auf dem Handy speichern.
Zuletzt diese Datei "öffnen" bzw. "teilen" oder an "phyphox kopieren".
Der Versuch erscheint unter den festeingestellten Versuchen.
